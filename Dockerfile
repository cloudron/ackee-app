FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# Upstream is using node:14-alpine https://github.com/nodejs/docker-node/blob/main/14/alpine3.17/Dockerfile#L3
ARG NODE_VERSION=14.21.3
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH="/usr/local/node-${NODE_VERSION}/bin:$PATH"

# renovate: datasource=github-releases depName=electerious/Ackee versioning=semver extractVersion=^v(?<version>.+)$
ARG ACKEE_VERSION=3.4.2

RUN curl -L https://github.com/electerious/Ackee/archive/v${ACKEE_VERSION}.tar.gz | tar -zxvf - --strip-components=1
RUN ln -sf /app/data/env /app/code/.env

RUN yarn install --production --frozen-lockfile && \
    npm run build

COPY env.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
