#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function login() {
        await browser.get('https://' + app.fqdn);
        await visible(By.xpath('//input[@placeholder="Username"]'));
        await browser.findElement(By.xpath('//input[@placeholder="Username"]')).sendKeys('admin');
        await browser.findElement(By.xpath('//input[@placeholder="Password"]')).sendKeys('changeme');
        await browser.findElement(By.xpath('//button[contains(text(), "Sign In")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//h2[contains(text(), "Active visitors")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/#/settings');
        await visible(By.xpath('//button/span[contains(text(), "Sign Out")]'));
        await browser.findElement(By.xpath('//button/span[contains(text(), "Sign Out")]')).click();
        await visible(By.xpath('//input[@placeholder="Username"]'));
    }

    async function addDomain() {
        await browser.get('https://' + app.fqdn + '/#/settings');
        await visible(By.xpath('//button/span[contains(text(), "New domain")]'));
        await browser.findElement(By.xpath('//button/span[contains(text(), "New domain")]')).click();
        await visible(By.xpath('//input[contains(@placeholder, "Domain title")]'));
        await browser.findElement(By.xpath('//input[contains(@placeholder, "Domain title")]')).sendKeys('cloudron.io');
        await browser.findElement(By.xpath('//button[contains(text(), "Add")]')).click();
        await visible(By.xpath('//button/span[contains(text(), "cloudron.io")]'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function domainExists() {
        await browser.get('https://' + app.fqdn + '/#/settings');
        await visible(By.xpath('//button/span[contains(text(), "cloudron.io")]'));
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('can add domain', addDomain);
    it('can logout', logout);

    it('restart app', function () {
        execSync('cloudron restart --app ' + app.id, EXEC_ARGS);
    });
    it('can login', login);
    it('can check domain', domainExists);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can check domain', domainExists);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can check domain', domainExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add domain', addDomain);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can check domain', domainExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
