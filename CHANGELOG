[0.1.0]
* Initial version

[0.2.0]
* Improve post install message

[1.0.0]
* Initial stable version
* Update Ackee to 3.0.5
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.0.5)

[1.1.0]
* Update Ackee to 3.1.0
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.1.0)
* Views and duration details: Click on a chart bar on the overview and insights page to see more details
* Percentage changes of average views and duration in the facts panel
* Active visitors counter updates periodically without reloading the UI

[1.1.1]
* Update Ackee to 3.1.1
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.1.1)
* Fix "Float cannot represent non numeric value: NaN" when visiting a new installation of Ackee

[1.2.0]
* Update Ackee to 3.2.0
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.2.0)
* Updated dependencies, including mongoose

[1.3.0]
* Update Ackee to 3.3.0
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.3.0)

[1.3.1]
* Update base image to 3.2.0

[1.3.2]
* Update Ackee to 3.3.1
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.3.1)

[1.4.0]
* Update Ackee to 3.4.0
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.4.0)
* Cache preflight requests (via Access-Control-Max-Age) (#261)
* Automatically add CORS headers for domains that have fully qualified domain names as titles (ACKEE_AUTO_ORIGIN) (#271)

[1.4.1]
* Update Acke tto 3.4.1
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.4.1)
* Vercel not attaching CORS headers because of unsupported multiValueHeaders (thanks @birjj, #330)
* ACKEE_AUTO_ORIGIN not attaching CORS headers (thanks @birjj, #330)

[1.4.2]
* Update Ackee to 3.4.2
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/electerious/Ackee/releases/tag/v3.4.2)
* Rendering issue in Safari

[1.4.3]
* Update base image to 4.2.0

[1.5.0]
* checklist added to manifest

