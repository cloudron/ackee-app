## About

Ackee is a self-hosted analytics tool that cares about privacy. We believe that you don't need to track every aspect of your visitors. Ackee keeps tracked data anonymized to avoid that users are identifiable, while still providing helpful insights. It's the right tool for everyone who doesn't need a full-featured marketing analytics platform like Google Analytics or Matomo.

## Features

* Self-hosted: Ackee runs on your own server and is 100% open-source
* Modern technologies: Lightweight Node.js and MongoDB architecture
* Beautiful: Minimal and focused interface
* No cookies: No unique user tracking and therefore no required cookie message
* Events: Track button clicks, newsletter subscriptions and more
* GraphQL API: Fully documented GraphQL API that allows you to build new tools upon Ackee

