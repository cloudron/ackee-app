#!/bin/bash

set -eu

if [[ ! -f /app/data/env ]]; then
    cp /app/pkg/env.template /app/data/env
fi

sed -e "s,ACKEE_MONGODB=.*,ACKEE_MONGODB=${CLOUDRON_MONGODB_URL}," -i /app/data/env

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

export NODE_ENV=production

echo "==> Starting Ackee"
exec gosu cloudron:cloudron npm run server

